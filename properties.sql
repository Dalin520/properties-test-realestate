-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:8889
-- Generation Time: Apr 08, 2019 at 08:28 AM
-- Server version: 5.7.25
-- PHP Version: 7.3.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `properties`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_cate`
--

CREATE TABLE `tbl_cate` (
  `id` int(11) NOT NULL,
  `name` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `img` text COLLATE utf8_unicode_ci NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tbl_cate`
--

INSERT INTO `tbl_cate` (`id`, `name`, `img`, `status`) VALUES
(1, 'Cottage', '1', 1),
(2, 'Manifactured', '1', 1),
(3, 'Family House', '1', 1),
(4, 'Townhouse', '1', 1),
(5, 'Apartment', '1', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_property`
--

CREATE TABLE `tbl_property` (
  `id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `property_type` int(11) NOT NULL,
  `category` int(11) NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `price` double NOT NULL,
  `photo` text COLLATE utf8_unicode_ci NOT NULL,
  `location` text COLLATE utf8_unicode_ci NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tbl_property`
--

INSERT INTO `tbl_property` (`id`, `title`, `property_type`, `category`, `description`, `price`, `photo`, `location`, `status`) VALUES
(1, 'Sky house', 2, 3, 'Beautiful sight seeing', 9879808, '73580604.jpg', 'Phnom Penh', 1),
(2, 'Cootatlejljdflam', 2, 2, 'yooooo', 98767, '73580604.jpg', ' USA, Los Angeles, 10777 Santa Monica Boulevard. House for rentActive', 1),
(3, 'yuio', 2, 2, 'mula', 12345345, '73580604.jpg', 'location', 1),
(4, 'yetw', 2, 2, 'fdasfdas', 2345, 'apartiment.jpg', 'location', 1),
(5, 'Cootaljldjfazzzzz', 1, 1, 'This elegantly renovated period home is immaculate inside and out! Enviably situated this home comprises formal sitting and dining rooms which lead to a huge, beautifully bright living/dining which achieves indoor-outdoor brilliance and incorporates an open-plan kitchen of consummate quality. Main bedroom with study/retreat, walk-in robes, ensuite and sublime courtyard is complemented by a similarly luxurious second downstairs bedroom, also enjoying ensuite, walk-in robes and study/lounge space. Upstairs, two further bedrooms & modern bathroom surround generous children’s living and sunny balcony. Glorious gardens include solar/gas heated pool & spa, inviting entertaining areas,double carport & additional parking. A prestigious position offering quick access to many leading schools including Camberwell Grammar School, Fintona & Camberwell Girls. Close to transport & shops. Garden & Pool maintenance included.', 212100, '5b3fbc21c8d6ed47008b45e0-750-563.jpg', 'location', 1),
(6, 'abcdfsafd', 1, 1, 'des', 777, '5b3fbc21c8d6ed47008b45e0-750-563.jpg', 'location', 1),
(7, 'Arana', 2, 5, 'Good place for business, crowded', 7773412, '5b3fbc21c8d6ed47008b45e0-750-563.jpg', 'st 271, #446, Phnom Penh Cambodia', 1),
(8, 'shy', 1, 1, 'des', 777, '290_West_St_3B_14.0.jpg', 'Watphnhom', 1),
(9, 'new', 2, 4, 'des', 999777, '290_West_St_3B_14.0.jpg', 'location', 1),
(10, 'jo', 2, 2, 'des', 777, '73580604.jpg', 'location', 1),
(11, 'nknkjnkj', 1, 1, 'dserasfdsfasf', 777, '5b3fbc21c8d6ed47008b45e0-750-563.jpg', ' USA, Los Angeles, 10777 Santa Monica Boulevard. House for rentActive', 1),
(12, 'iiiii', 2, 2, 'erer', 12345, '5b3fbc21c8d6ed47008b45e0-750-563.jpg', ' USA, Los Angeles, 10777 Santa Monica Boulevard. House for rentActive', 1),
(13, 'io', 2, 2, 'jjjj', 21111, '73580604.jpg', 'location', 1),
(14, 'Relaxing Muschew', 2, 4, 'Amazing place with fresh air', 10000000, '5b3fbc21c8d6ed47008b45e0-750-563.jpg', 'Averest', 1),
(15, 'abcdfs22222afd', 1, 1, 'des', 777, 'apartiment.jpg', 'location', 1),
(16, 'Mr Dorot', 2, 1, 'good environment, .', 123456, '290_West_St_3B_14.0.jpg', 'Battam Bang st231, #89', 1),
(17, 'dfag12', 1, 1, 'dfas', 123456, '5b3fbc21c8d6ed47008b45e0-750-563.jpg', 'location', 1),
(18, 'dfdfasfdsss', 2, 2, 'Boheme Apartments, set at the waterside gateway to CityVillage, offer the ultimate urban Gold City lifestyle allowing you to live life your way at the heart of the city. Boheme Apartments offer inspiring interior design and functional open plan layouts th', 999865, '290_West_St_3B_14.0.jpg', 'Austria, Vienna, Fleischmarkt 20. House for saleActive', 1),
(19, 'ooodfag12', 1, 1, 'Boheme Apartments, set at the waterside gateway to CityVillage, offer the ultimate urban Gold City lifestyle allowing you to live life your way at the heart of the city. Boheme Apartments offer inspiring interior design and functional open plan layouts that flow seamlessly through to generous b', 123456, 'Screen Shot 2019-04-08 at 2.53.05 PM.png', 'location', 1),
(20, 'Apartments at City', 2, 2, 'Boheme Apartments, set at the waterside gateway to CityVillage, offer the ultimate urban Gold City lifestyle allowing you to live life your way at the heart of the city. Boheme Apartments offer inspiring interior design and functional open plan layouts that flow seamlessly through to generous balconies, with charming Gold Coas', 8123456, '5b3fbc21c8d6ed47008b45e0-750-563.jpg', 'location', 1),
(21, 'why tell', 2, 2, 'dfas', 123456, '73580604.jpg', 'location', 1),
(22, 'jjjjjjj', 2, 2, 'jjjjj', 42423, '5b3fbc21c8d6ed47008b45e0-750-563.jpg', ' USA, Los Angeles, 10777 Santa Monica Boulevard. House for rentActive', 1),
(23, 'CityVillage', 2, 2, 'Apartments  set at the waterside gateway to CityVillage, offer the ultimate urban Gold City lifestyle allowing you to live life your way at the heart of the city. Boheme Apartments offer inspiring interior design and functional open plan layouts that flow seamlessly through to generous balconies, with charming', 61000000, 'apartiment.jpg', 'Austria, Vienna, Fleischmarkt 20. House for saleActive', 1),
(24, 'Borey Nara', 1, 3, 'Boheme Apartments, set at the waterside gateway to CityVillage, offer the ultimate urban Gold City lifestyle allowing you to live life your way at the heart of the city. Boheme Apartments offer inspiring interior design and functional open plan l\r\nBoheme Apartments, set at the waterside gateway to CityVillage, offer the ultimate urban Gold City lifestyle allowing you to live life your way at the heart of the city. Boheme Apartments offer inspiring interior design and functional open plan\r\nBoheme Apartments, set at the waterside gateway to CityVillage, offer the ultimate urban Gold City lifestyle allowing you to live life your way at the heart of the city. Boheme Apartments offer inspiring interior design and functional open plan l\r\nBoheme Apartments, set at the waterside gateway to CityVillage, offer the ultimate urban Gold City lifestyle allowing you to live life your way at the heart of the city. Boheme Apartments offer inspiring interior design and functional open plan\r\nBoheme Apartments, set at the waterside gateway to CityVillage, offer the ultimate urban Gold City lifestyle allowing you to live life your way at the heart of the city. Boheme Apartments offer inspiring interior design and functional open plan l\r\nBoheme Apartments, set at the waterside gateway to CityVillage, offer the ultimate urban Gold City lifestyle allowing you to live life your way at the heart of the city. Boheme Apartments offer inspiring interior design and functional open plan', 9088732, '73580604.jpg', 'Phnom Penh Cambodia', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_property_type`
--

CREATE TABLE `tbl_property_type` (
  `id` int(11) NOT NULL,
  `name` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `img` text COLLATE utf8_unicode_ci NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tbl_property_type`
--

INSERT INTO `tbl_property_type` (`id`, `name`, `img`, `status`) VALUES
(1, 'Buy', '1', 1),
(2, 'Rent', '1', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_cate`
--
ALTER TABLE `tbl_cate`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_property`
--
ALTER TABLE `tbl_property`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_property_type`
--
ALTER TABLE `tbl_property_type`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_cate`
--
ALTER TABLE `tbl_cate`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `tbl_property`
--
ALTER TABLE `tbl_property`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `tbl_property_type`
--
ALTER TABLE `tbl_property_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
