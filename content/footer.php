<!-- footer section -->
<div class="wrapper-div footer-bg border-top-red">
    <div class="footer-wrapper">
        <div class="container">
            <div style="width:55%;margin:auto;background-color:green;">
                <div class="footer-section">
                    About Us
                    <div class="border-btm"></div>
                    <p>
                        Welcome to our website where provides you a best choice to buy or rent house.
                    </p>
                </div>

                <div class="footer-section" style="float:right">
                    Contact Us
                    <div class="border-btm"></div>
                    <p>Please feel free to contact us by:</p>
                    <a href="#" style="color:black">+855 12345678</a><br>
                    <a href="#" style="color:black">+855 98765432</a>
                    <p>or:</p>
                    <a href="#" style="color:black">company_name@mail.com</a><br>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-btm">
        <div class="container">
            <center>
                <img src="img/logo/properties-red.png" width="55" alt="">
            </center>
            <h6>Properties test assignment. 2019</h6>
        </div>
    </div>
</div>