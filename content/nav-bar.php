<!-- section nav bar -->
<div class="w-100 top-bar box-shadow bg-nav ">
    <div class="container">
        <div class="wrapper-logo">
            <a href="index.php">
                <div class="wrapper-img-width">
                    <img src="img/logo/properties.png" alt="">
                </div>
            </a>
        </div>
        <div class="btn-menu">
            <div class="menu-div"></div>
            <div class="menu-div"></div>
            <div class="menu-div"></div>
        </div>
        <div class="nav-bar">
            <ul>
                <li><a href="index.php"><i class="fa fa-home text-white"></i></a></li>
                <li><a href="index.php?type=1">Buy</a></li>
                <li><a href="index.php?type=2">Rent</a></li>
                <li><a href="contact.php" class="contact">Contact us</a></li>
            </ul>
        </div>
    </div>
</div>