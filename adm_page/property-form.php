<?php
  include("../content/adm-head.php");
  include('../db_connection/database.php');
  $db=new Db;
  $db->connect(); 
  
  $path_id=$_GET['edit'];

?>    
</head>
<body>
    <!-- top nav logo -->
    <nav class="navbar navbar-dark fixed-top bg-dark flex-md-nowrap p-0 shadow">
        <a class="navbar-brand col-sm-3 col-md-2 mr-0" href="#">
            <img src="../img/logo/properties.png" alt="" width='70'>
        </a>
    </nav>
    <!-- end top nav logo -->
    
    <div class="container-fluid">
        <div class="row">
           <!-- section side menu -->
           <nav class="col-md-2 d-none d-md-block bg-light sidebar">
                <div class="sidebar-sticky">
                    <ul class="nav flex-column">
                        <li class="nav-item" id="countries">
                            <a class="nav-link" href="#"><i class="fas fa-home fa-sm pr-3"></i>Home</a>
                        </li>
                        <li class="nav-item" id="countries">
                            <a class="nav-link" href="#"><i class="fas fa-dice-five fa-sm pr-3"></i>Type</a>
                        </li>
                        <li class="nav-item" id="countries">
                            <a class="nav-link" href="#"><i class="fas fa-list-ul pr-3"></i>Category</a>
                        </li>
                        <li class="nav-item" id="countries">
                            <a class="nav-link active-link" href="add-property.php"><i class="fas fa-store fa-sm pr-3"></i>Property</a>
                        </li>
                    </ul>
                </div>
            </nav>
            <!-- end section side menu -->
            <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4"><div class="chartjs-size-monitor" style="position: absolute; left: 0px; top: 0px; right: 0px; bottom: 0px; overflow: hidden; pointer-events: none; visibility: hidden; z-index: -1;"><div class="chartjs-size-monitor-expand" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;"><div style="position:absolute;width:1000000px;height:1000000px;left:0;top:0"></div></div><div class="chartjs-size-monitor-shrink" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;"><div style="position:absolute;width:200%;height:200%;left:0; top:0"></div></div></div>
            <div class="d-flex body-wrap justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                <input type="hidden" value="1" id="page_num" />
                <h1 class="h2">Add House</h1>
                <a href="#">
                    <img src="../img/logo/properties-red.png" alt="" width='250'>
                </a>
            </div>
            <div>
              <form method="post" class='form-upl' action='./action/save-property.php' enctype="multipart/form-data">
              <input type="hidden" id="txt_edit_no" name="txt_edit_no" value="0" />  
                <?php
                  if(!$path_id)
                  {
                    ?>
                      <div class="form-group">
                        <label for="name">Title</label>
                        <input type="text" class="form-control name-valid name" name="txt_title" id="txt_title" placeholder="Title" autofocus>
                      </div>
                      <div class="form-group">
                        <label>Type</label>
                        <select id="txt_type" name="txt_type" class="form-control">
                          <option value="0">---- Property Type ----</option>
                          <?php
                          $sql="SELECT * FROM tbl_property_type WHERE status=1";
                          $result=$db->cnn->query($sql);
                          while($row=$result->fetch_array())
                          {
                          ?>
                              <option value="<?php echo $row[0]; ?>">
                                  <?php echo $row[1]; ?>
                              </option>	
                          <?php
                          }
                          ?>
                        </select>
                      </div>
                      <div class="form-group">
                        <label>Category</label>
                        <select id="txt_cate" name="txt_cate" class="form-control">
                          <option value="0">---- Select Category----</option>
                          <?php
                          $sql="SELECT * FROM tbl_cate WHERE status=1";
                          $result=$db->cnn->query($sql);
                          while($row=$result->fetch_array())
                          {
                          ?>
                              <option value="<?php echo $row[0]; ?>">
                                  <?php echo $row[1]; ?>
                              </option>	
                          <?php
                          }
                          ?>
                        </select>
                      </div>
                      <div class="form-group">
                        <label for="capital">Description</label>
                        <textarea name="txt_des" id="txt_des" cols="30" class="form-control" rows="10"></textarea>  
                      </div>
                      <div class="form-group">
                        <label for="region">Price</label>
                        <input type="text" class="form-control name-valid txt_price" name="txt_price" id="txt_price" placeholder="$">
                      </div>
                      <div class="form-group">
                        <label for="region">Location</label>
                        <input type="text" class="form-control name-valid txt_location" name="txt_location" id="txt_location" placeholder="Location">
                      </div>
                      <label>photo</label>
                      <input type="file" id='photo' name='photo'
                            accept=".jpg,.jpeg,.png"
                            placeholder="Select property's photo" />
                    <?php
                  }
                  else{
                    
                    $sql="SELECT * FROM tbl_property WHERE id='".$path_id."' ";
                    $result=$db->cnn->query($sql);
                    while($row=$result->fetch_array())
                    {
                    ?>
                      <div class="form-group">
                        <label for="name">Title</label>
                        <input type="text" class="form-control name-valid name" value="<?php echo $row[1]; ?>" name="txt_title" id="txt_title" autofocus>
                      </div>
                      <div class="form-group">
                        <label>Type</label>
                        <select id="txt_type" name="txt_type" class="form-control">
                          <option value="0">---- Property Type ----</option>
                          <?php
                          $sql_type="SELECT * FROM tbl_property_type WHERE status=1";
                          $result_type=$db->cnn->query($sql_type);
                          while($row_type=$result_type->fetch_array())
                          {
                            if($row_type[0]==$row[2])
                            {
                              ?>
                                <option value="<?php echo $row_type[0];?>" selected>
                                    <?php echo $row_type[1]; ?>
                                </option>
                              <?php
                            }  
                            else{
                              ?>
                                <option value="<?php echo $row_type[0];?>">
                                    <?php echo $row_type[1]; ?>
                                </option>
                              <?php
                            }
                          }
                          ?>
                        </select>
                      </div>
                      <div class="form-group">
                        <label>Category</label>
                        <select id="txt_cate" name="txt_cate" class="form-control">
                          <option value="0">---- Select Category----</option>
                          <?php
                          $sql_cate="SELECT * FROM tbl_cate WHERE status=1";
                          $result_cate=$db->cnn->query($sql_cate);
                          while($row_cate=$result_cate->fetch_array())
                          {
                            if($row_cate[0]==$row[2])
                            {
                              ?>
                                <option value="<?php echo $row_cate[0];?>" selected>
                                    <?php echo $row_cate[1]; ?>
                                </option>
                              <?php
                            }  
                            else{
                              ?>
                                <option value="<?php echo $row_cate[0];?>">
                                    <?php echo $row_cate[1]; ?>
                                </option>
                              <?php
                            }
                          }
                          ?>
                        </select>
                      </div>	
                      <div class="form-group">
                        <label for="capital">Description</label>
                        <textarea name="txt_des" id="txt_des" cols="30" class="form-control" rows="10"><?php echo $row[4]; ?></textarea>  
                      </div>
                      <div class="form-group">
                        <label for="region">Price</label>
                        <input type="text" class="form-control name-valid txt_price" value="<?php echo $row[5]; ?>" name="txt_price" id="txt_price" placeholder="$">
                      </div>
                      <div class="form-group">
                        <label for="region">Location</label>
                        <input type="text" class="form-control name-valid txt_location" value="<?php echo $row[7]; ?>" name="txt_location" id="txt_location" placeholder="Location">
                      </div>
                      <label>photo</label>
                      <input type="file" id='photo' name='photo'
                            value="../img/<?php echo $row[6]; ?>"
                            accept=".jpg,.jpeg,.png"
                            placeholder="Select property's photo" />
                    <?php
                    }
                    
                  }
                ?>

                <div class='mt-5'></div>
                <button type="submit" name="submit" class="btn btn-success btn-lg mr-2"><i class='fas fa-save pr-2'></i> Save</button>
                <button type="button" class="btn btn-secondary backLink btn-lg"><i class='fas fa-arrow-left pr-2'></i> Back</button>              
              
              </form>
            </div>
            </main>
        </div>
        <!--  -->

        ?>
        </div>
    <script src="../js/jQuery/jquery-3.3.1.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/feather-icons/4.9.0/feather.min.js"></script>

    <script>
      $(document).ready(function(){
          $('.backLink').click(function(){
            window.history.back();
          });
        $('.name').on('keypress', function(e) {
            var regex = new RegExp("^[a-zA-Z ]*$");
            var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
            if (regex.test(str)) {
                console.log('true')
                return true;
            }
            e.preventDefault();

            return false;
        });
        $('.txt_price').on('keypress', function(e) {
            var regex = new RegExp("^[0-9 ]*$");
            var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
            if (regex.test(str)) {
                console.log('true')
                return true;
            }
            e.preventDefault();

            return false;
        });
        $('.from-upl').submit(function(e){
            return false;
        });

        //get current path variable from url
        var url = window.location.search;
        var id =url.split('=');
        if(url){
          $('#txt_edit_no').val(id[1]);
        }
        else{
          $('#txt_edit_no').val(0);
        }
      });
    </script>
</body>
</html>