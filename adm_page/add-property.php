<?php
    include("../content/adm-head.php");
    include('../db_connection/database.php');
    $db=new Db;
    $db->connect();

?>
    
</head>
<body>
     <!-- top nav logo -->
     <nav class="navbar navbar-dark fixed-top bg-dark flex-md-nowrap p-0 shadow">
        <a class="navbar-brand col-sm-3 col-md-2 mr-0" href="#">
            <img src="../img/logo/properties.png" alt="" width='70'>
        </a>
    </nav>
    <!-- end top nav logo -->
    
    <div class="container-fluid">
        <div class="row">
            <!-- start side bar: left menu -->
            <nav class="col-md-2 d-none d-md-block bg-light sidebar">
                <div class="sidebar-sticky">
                    <ul class="nav flex-column">
                        <li class="nav-item" id="countries">
                            <a class="nav-link" href="index.php"><i class="fas fa-home fa-sm pr-3"></i>Home</a>
                        </li>
                        <li class="nav-item disabled" id="countries">
                            <a class="nav-link" href="#"><i class="fas fa-dice-five fa-sm pr-3"></i>Type</a>
                        </li>
                        <li class="nav-item disabled" id="countries">
                            <a class="nav-link" href="#"><i class="fas fa-list-ul pr-3"></i>Category</a>
                        </li>
                        <li class="nav-item" id="countries">
                            <a class="nav-link active-link" href="#"><i class="fas fa-store fa-sm pr-3"></i>Property</a>
                        </li>
                    </ul>
                </div>
            </nav>
            <!-- end side bar: left menu -->

            <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4"><div class="chartjs-size-monitor" style="position: absolute; left: 0px; top: 0px; right: 0px; bottom: 0px; overflow: hidden; pointer-events: none; visibility: hidden; z-index: -1;"><div class="chartjs-size-monitor-expand" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;"><div style="position:absolute;width:1000000px;height:1000000px;left:0;top:0"></div></div><div class="chartjs-size-monitor-shrink" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;"><div style="position:absolute;width:200%;height:200%;left:0; top:0"></div></div></div>
            <div class="d-flex body-wrap justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                <input type="hidden" value="1" id="page_num" />
                <h1 class="h2">Property Info</h1>
                <a href="#">
                    <img src="../img/logo/properties-red.png" alt="" width='250'>
                </a>
            </div>
            <div>
                <a href="./property-form.php">
                    <button type="button" class="btn btn-primary btn-lg mb-3"><span class="btn-label"><i class="fa fa-plus"></i></span>&nbsp; Add House</button>
                </a>
                <table class="table">
                    <thead>
                    <tr>
                        <th scope="col">ID</th>
                        <th scope="col">Title</th>
                        <th scope="col">Property Type</th>
                        <th scope="col">Category</th>
                        <th scope="col">Description</th>
                        <th scope="col">Price</th>
                        <th scope="col">Photo</th>
                        <th scope="col">Location</th>
                        <th scope="col">Status</th>
                        <th scope="col">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                        $sql = "SELECT * FROM tbl_property ORDER BY id DESC";
                        $result = $db->cnn->query($sql);
                        while($row=$result->fetch_array())
                        {
                            ?>
                                <tr>
                                    <td><?php echo $row[0]; ?></td>
                                    <td><?php echo $row[1]; ?></td>
                                    <td>
                                        <?php
                                            $sqlType="SELECT * FROM tbl_property_type WHERE id='".$row[2]."' ";
                                            $resultType=$db->cnn->query($sqlType);
                                            while($rowType=$resultType->fetch_array())
                                            {?>
                                                <input type="hidden" value="<?php echo $rowType[0];?>"><?php echo $rowType[1]; ?>
                                            <?php
                                            }
                                        ?>
                                    </td>
                                    <td>
                                        <?php
                                            $sqlType="SELECT * FROM tbl_cate WHERE id='".$row[3]."' ";
                                            $resultType=$db->cnn->query($sqlType);
                                            while($rowType=$resultType->fetch_array())
                                            {?>
                                                <input type="hidden" value="<?php echo $rowType[0];?>"><?php echo $rowType[1]; ?>
                                            <?php
                                            }
                                        ?>
                                    </td>
                                    <td>
                                        <span class="d-inline-block text-truncate" style="max-width: 150px;">
                                            <?php echo $row[4]; ?>
                                        </span>
                                    </td>
                                    <td><?php echo $row[5]; ?>$</td>
                                    <td>
                                        <input type="hidden" value="<?php echo $row[6]; ?>">
                                        <img src="../img/properties/<?php echo $row[6]; ?>" width="50"/>
                                    </td>
                                    <td>
                                        <span class="d-inline-block text-truncate" style="max-width: 150px;">
                                            <?php echo $row[7]; ?>
                                        </span>
                                    </td>
                                    <td>
                                        <?php
                                            if($row[8]==1)
                                            {
                                            ?>
                                                <a href="add-property.php?id=<?php echo $row[0];?>&status=<?php echo $row[8]; ?>">
                                                    <input type="hidden" value="<?php echo $row[8]; ?>">
                                                    <img src="../img/active.png" width="40" ? class='img_st1'>
                                                </a>
                                            <?php
                                            }
                                            else
                                            {
                                            ?>
                                                 <a href="add-property.php?id=<?php echo $row[0];?>&status=<?php echo $row[8]; ?>">
                                                    <input type="hidden" value="<?php echo $row[8]; ?>">
                                                    <img src="../img/inactive.png" width="40" name="<?php echo $row[8]; ?>" class='img_st'>
                                                </a>
                                            <?php
                                            }
                                        ?>
                                    </td>
                                    
                                    <td>
                                        <a href="./property-form.php?edit=<?php echo $row[0]; ?>"><button class="btn btn-success" id="btn_edit"><i class="fas fa-edit"></i></button></a>
                                    </td>
                                </tr>
                            <?php
                            }
                        ?>
                    </tbody>
                </table>

            </div>
         
            </main>
        </div>
        <?php
            // when click on btn status then change its status
            if($_GET['id'])
            {
                if($_GET['status']==1)
                {
                    $sql="UPDATE tbl_property SET status=2 WHERE id=".$_GET['id']."";
                    
                }
                else if($_GET['status']==2)
                {
                    $sql="UPDATE tbl_property SET status=1 WHERE id=".$_GET['id']."";
                }
                $db->cnn->query($sql);
            }
        ?>
        </div>

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/feather-icons/4.9.0/feather.min.js"></script>
    <script>
        $(document).ready(function() {
            //change status image whenever the btn status is triggered
            
            $(".nav li.disabled a").click(function() {
                return false;
            });
        });
    </script>
</body>
</html>
