<?php
    include('../content/adm-head.php');
?>
</head>
<body>
    <!-- top nav logo -->
    <nav class="navbar navbar-dark fixed-top bg-dark flex-md-nowrap p-0 shadow">
        <a class="navbar-brand col-sm-3 col-md-2 mr-0" href="#">
            <img src="../img/logo/properties.png" alt="" width='70'>
        </a>
    </nav>
    <!-- end top nav logo -->
    <div class="container-fluid">
        <div class="row">
            <!-- section side menu -->
            <nav class="col-md-2 d-none d-md-block bg-light sidebar">
                <div class="sidebar-sticky">
                    <ul class="nav flex-column">
                        <li class="nav-item" id="countries">
                            <a class="nav-link active-link" href="#"><i class="fas fa-home fa-sm pr-3"></i>Home</a>
                        </li>
                        <li class="nav-item" id="countries">
                            <a class="nav-link" href="#"><i class="fas fa-dice-five fa-sm pr-3"></i>Type</a>
                        </li>
                        <li class="nav-item" id="countries">
                            <a class="nav-link" href="#"><i class="fas fa-list-ul pr-3"></i>Category</a>
                        </li>
                        <li class="nav-item" id="countries">
                            <a class="nav-link" href="add-property.php"><i class="fas fa-store fa-sm pr-3"></i>Property</a>
                        </li>
                    </ul>
                </div>
            </nav>
            <!-- end section side menu -->

            <main role="main" class="d-block col-md-9 ml-sm-auto col-lg-10 px-4"><div class="chartjs-size-monitor" style="position: absolute; left: 0px; top: 0px; right: 0px; bottom: 0px; overflow: hidden; pointer-events: none; visibility: hidden; z-index: -1;"><div class="chartjs-size-monitor-expand" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;"><div style="position:absolute;width:1000000px;height:1000000px;left:0;top:0"></div></div><div class="chartjs-size-monitor-shrink" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;"><div style="position:absolute;width:200%;height:200%;left:0; top:0"></div></div></div>
            <div class="d-flex body-wrap justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                <input type="hidden" value="1" id="page_num" />
                <h1 class="h2">Welcome to Properties Dashboard</h1>
                <a href="#">
                    <img src="../img/logo/properties-red.png" alt="" width='250'>
                </a>
            </div>
            <div>
            <!-- Main menu in Dashborad. There are 2 menus BUY and SELL -->
            <div class='row pt-5'>
                <div class="col-sm-3 offset-sm-4 box-menu">
                    <center>
                        <a href="add-property.php" >
                            <img src="../img/house.png" alt="" width="250">
                            <h2>Property</h2>
                        </a>
                    </center>
                </div>
                <!-- <div class=" offset-sm-1 col-sm-3 box-menu">
                    <a href="#">
                        <center>
                            <img src="../img/house.png" alt="" width="250">
                            <h2>Category</h2>
                        </center>
                    </a>
                </div> -->
            </div>

            </div>

            </main>
        </div>
        </div>

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/feather-icons/4.9.0/feather.min.js"></script>
    
</body>
</html>
