$(document).ready(function(){
    var btn_num = 0;
    var navbar=$('.nav-bar');
    $('.btn-menu').click(function(){
        navbar.slideToggle()
        
    });
    // method getWindowInnerSize check if window's width is up than 992px then show Menu bar in different window size
    function getWindowInnerSize(){
        var w = window.innerWidth;
        var ww = document.getElementById("wis");
        ww.value = w;
        width = $('#wis').val();
        console.log(width);
    }
    setInterval(getWindowInnerSize,500);
    function checkNavMedia(){
        if(width>992){
            navbar.show()
        }   
    }
    setInterval(checkNavMedia,500);

    // btn scroll to top
    window.onscroll = function() {scrollFunction()};

    function scrollFunction() {
        if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
            document.getElementById("btn-scroll-top").style.display = "block";
        } else {
            document.getElementById("btn-scroll-top").style.display = "none";
        }
    }

    // When the user clicks on the button, scroll to the top of the document
    function topFunction() {
        document.body.scrollTop = 0;
        document.documentElement.scrollTop = 0;
    }
    document.getElementById('btn-scroll-top').onclick=function(){topFunction()};

});