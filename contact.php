<?php
    include("./content/home-head.php");
?>    
<title>Contact</title>

</head>
<body>     
    <button class="btn-scroll-top" id="btn-scroll-top" ><i class="fas fa-arrow-circle-up"></i></button> 
    <div class="container-fluid">
        <!-- top-bar -->
        <?php include('./content/nav-bar.php');?>  
        <!-- end top-bar  -->
        <!-- start content -->
        <div class="wrapper-div mt-100">
            <input type="hidden" id="wis" value=""><!-- don't delete this line because it is return the menu display status -->
            <div class="container bg-white radius">
                <div class="wrapper-con-box1 mb-50">
                    <h2>Contact us</h2>
                    <p>Please feel free to contact us by</p>
                    <br>
                    <p>
                        <i class="fas fa-at"></i>
                        <a href="#">company_name@mail.com</a>
                        <a href="#">company_name@mail.com</a>
                    </p>
                    <p>
                        <i class="fas fa-phone"></i>
                        <a>+855 123456789</a>
                        <a>+855 987654321</a>
                    </p>
                </div>
                <div class="wrapper-con-photo">
                    <img src="./img/house.png">
                </div>
                <div class="form-box shadow-right-side mb-50">                    
					<h2>Message us</h2>
					<form>
						<input type="text" placeholder="username...">
						<input type="email" placeholder="email...">
						<textarea placeholder="write here..."></textarea>
						<input type="submit" value="send">
					</form>					
                </div>               
            </div>
        </div>

        <!-- end content -->

        <!-- start footer -->
        <?php
            include('./content/footer.php');
        ?>
        <!-- end footer -->

    <!-- Script Library -->
    <script src="js/jQuery/jquery-3.3.1.js"></script>
    

    <!-- Customize Script -->
    <script src="js/myScript/script.js"></script>
    <script>
        $(document).ready(function(){
           
        });
    </script>
</body>
</html>