<?php
    include("./content/home-head.php");
    include('./db_connection/database.php');
    $db=new Db;
    $db->connect(); 
    
?>
    <title>Properties</title>
</head>

<body>
    <button class="btn-scroll-top" id="btn-scroll-top">
        <i class="fas fa-arrow-circle-up"></i>
    </button>

    <div class="container-fluid">

        <!-- top-bar -->
        <?php
            include('./content/nav-bar.php');
        ?>
        <!-- end top-bar  -->
        <!-- start content -->
        <div class="wrapper-div">
            <input type="hidden" id="wis" value=""><!-- don't delete this line because it is return the menu display status -->
            <div class="container bg-white">
                
                <?php
                    // check if there is var id is existed (show detail)
                    if(isset($_GET['id']))
                    {
                        ?>
                        <!-- start content -->
                        <div class="mt-100 mb-40">
                            <input type="hidden" id="wis" value=""><!-- don't delete this line because it is return the menu display status -->
                            <div class="bg-white radius" style='padding:50px;'>
                                <?php
                                   $sql="SELECT * FROM tbl_property WHERE id='".$_GET['id']."' ";
                                   $result=$db->cnn->query($sql);
                                   while($row=$result->fetch_array())
                                    {
                                        ?>
                                        <div class="box-detail-1">
                                            <p><?php echo $row[1] ?></p>
                                            <span style="color:#D05C58;"><i class="fas fa-dollar-sign"></i>&nbsp; <?php echo $row[5]; ?> </span>
                                            <br>
                                            <span>
                                                <?php
                                                    $sql_cate="SELECT * FROM tbl_cate WHERE id='".$row[3]."' ";
                                                    $result_cate = $db->cnn->query($sql_cate);
                                                    while($row_cate=$result_cate->fetch_array())
                                                    {
                                                        echo $row_cate[1];
                                                    }
                                                ?>
                                                For
                                                <?php
                                                    $sql_cate="SELECT * FROM tbl_property_type WHERE id='".$row[2]."' ";
                                                    $result_cate = $db->cnn->query($sql_cate);
                                                    while($row_cate=$result_cate->fetch_array())
                                                    {
                                                        echo $row_cate[1];
                                                    }
                                                ?>
                                            
                                            </span>
                                            <br><br/>
                                            <span><i class="fas fa-map-marker"></i>&nbsp; <?php echo $row[7]; ?></span>
                                        </div>
                                        <div class="img-box">
                                            <img src="./img/properties/<?php echo $row[6] ?>" alt="">
                                        </div>
                                        <div class="box-detail-2">
                                            <p>Description</p>
                                            <span><?php echo $row[4]; ?></span>
                                        </div>
                                        <?php
                                    }
                                ?>
                            </div>
                        </div>
                <?php
                    }
                    //check if there is type var is existed which user clicked on menu of any cates(Property_type: like buy or rent)
                    else if(isset($_GET['type']))
                    {
                        ?>
                            <div class="wrapper-content-box-submenu shadow-right-side mt-100 radius">
                            <!-- Retrieve data all of Buy property type -->
                            <?php
                            $sql="SELECT * FROM tbl_property WHERE status=1 AND property_type='".$_GET['type']."' ORDER BY id DESC";
                            $result=$db->cnn->query($sql);
                            while($row=$result->fetch_array())
                            {
                                ?>
                                        <a href="./index.php?id=<?php echo $row[0]; ?>">
                                            <div class="wrapper-content-box">
                                                <div class="content-img" style="position:relative;background-image:url('./img/properties/<?php echo $row[6]; ?>');">
                                                    <div class="tag">
                                                        For
                                                        <?php
                                                            $sql_cate="SELECT * FROM tbl_property_type WHERE id='".$row[2]."' ";
                                                            $result_cate = $db->cnn->query($sql_cate);
                                                            while($row_cate=$result_cate->fetch_array())
                                                            {
                                                                echo $row_cate[1];
                                                            }
                                                        ?>
                                                    </div>
                                                </div>
                                                <div class="content-detail">
                                                    <span><?php echo $row[1]; ?></span><br>
                                                    <p><i class="fas fa-map-marker"></i>&nbsp;<?php echo $row[7]; ?></p>
                                                    <p><i class="fas fa-tag"></i>&nbsp;
                                                        <?php
                                                            $sql_cate="SELECT * FROM tbl_cate WHERE id='".$row[3]."' ";
                                                            $result_cate = $db->cnn->query($sql_cate);
                                                            while($row_cate=$result_cate->fetch_array())
                                                            {
                                                                echo $row_cate[1];
                                                            }
                                                        ?>
                                                    </p>
                                                    <p><i class="fas fa-dollar-sign"></i>&nbsp;<?php echo $row[5]; ?></p>
                                                </div>
                                            </div>
                                        </a>
                                <?php
                            }
                            ?>
                            
                            </div>
                        <?php
                    }
                    //else when use didnt any card of property so the is id var in not existed yet
                    else{
                        ?>
                        <!-- slide section -->
                        <div class="section-right">
                            <!-- Slide -->
                            <div class="slide-box bg-white">
                                <div class="wrapper-slide w-100">
                                    <img src="img/bg-home.png" style="width:100%;" alt="">
                                </div>
                                <div class="wrapper-slide-title">
                                    <h1 class="text-shadow">Are you looking for properties?</h1>
                                    <h4 class="text-shadow">Your first making decision should be started from here.</h4>
                                </div>
                                <div class="wrapper-slide-photo" style='z-index:2;width:190px;'>
                                    <img src="./img/house.png" alt="">
                                </div>
                                <div class="wrapper-slide-photo" style="right:310px;bottom:160px;z-index:1">
                                    <img src="./img/house.png" alt="">
                                </div>
                            </div>
                        </div>
                        
                        <!-- purpose website section -->
                        <div class="purpose-section">
                            <center>
                                <h1>Welcome to our website</h1><br/>
                                <span>Thank you for being here where you will be aware of properties and provides services to buy and rent houses.</span>
                            </center>

                        </div>
                        <!-- box content section -->
                        <div class="section-content bg-white">
                            <div class="wrapper-submenu">
                                <div class="wrapper-submenu-header">
                                    <div class="wrapper-submenu-header-style">
                                        <a href="content/kh-content.html" class="no-text-decoration">
                                            <span id="kh-content" class="sub-content">
                                                <i class="fas fa-store fa-sm pr-2"></i> Our Hot Offers
                                            </span>
                                        </a>
                                       
                                    </div>
                                </div>
                                <div class="wrapper-content-box-submenu shadow-right-side">
                                <!-- Retrieve data all of Buy property type -->
                                <?php
                                $sql="SELECT * FROM tbl_property WHERE status=1 ORDER BY id DESC";
                                $result=$db->cnn->query($sql);
                                while($row=$result->fetch_array())
                                {
                                    ?>
                                            <a href="./index.php?id=<?php echo $row[0]; ?>">
                                                <div class="wrapper-content-box">
                                                    <div class="content-img" style="position:relative;background-image:url('./img/properties/<?php echo $row[6]; ?>');">
                                                        <div class="tag">
                                                            For
                                                            <?php
                                                                $sql_cate="SELECT * FROM tbl_property_type WHERE id='".$row[2]."' ";
                                                                $result_cate = $db->cnn->query($sql_cate);
                                                                while($row_cate=$result_cate->fetch_array())
                                                                {
                                                                    echo $row_cate[1];
                                                                }
                                                            ?>
                                                        </div>
                                                    </div>
                                                    <div class="content-detail">
                                                        <span><?php echo $row[1]; ?></span><br>
                                                        <p><i class="fas fa-map-marker"></i>&nbsp;<?php echo $row[7]; ?></p>
                                                        <p><i class="fas fa-tag"></i>&nbsp;
                                                            <?php
                                                                $sql_cate="SELECT * FROM tbl_cate WHERE id='".$row[3]."' ";
                                                                $result_cate = $db->cnn->query($sql_cate);
                                                                while($row_cate=$result_cate->fetch_array())
                                                                {
                                                                    echo $row_cate[1];
                                                                }
                                                            ?>
                                                        </p>
                                                        <p><i class="fas fa-dollar-sign"></i>&nbsp;<?php echo $row[5]; ?></p>
                                                    </div>
                                                </div>
                                            </a>
                                    <?php
                                }
                                ?>
                                
                                </div>
                                
                            </div>
                            
                        </div>
                        <?php
                    }
                ?>
            
            </div>
        </div>

        <!-- end content -->

        <!-- start footer -->
        <?php include('./content/footer.php'); ?>
        <!-- end footer -->

        <!-- Script Library -->
        <script src="js/jQuery/jquery-3.3.1.js"></script>


        <!-- Customize Script -->
        <script src="js/myScript/script.js"></script>

</body>

</html>
